import pygame
from config import *
import sys
import json
import random

pygame.init()
screen = pygame.display.set_mode(WINDOW_SIZE)
pygame.display.set_caption("Gallows Game")
clock = pygame.time.Clock()
font = pygame.font.SysFont("Arial", 50) # font object
font.set_bold(True)


class Letter():
    def __init__(self, letter, color,size, coords):
        self.letter = letter
        self.color = color
        self.size = size
        self.text = font.render(self.letter, True, self.color)
        self.surf = pygame.Surface((self.size,self.size), pygame.SRCALPHA)
        self.surf.blit(self.text,(self.surf.get_size()[0]//2-self.text.get_size()[0]//2,0))
        self.coords = coords
        self.active = True

    def blit(self):
        screen.blit(self.surf, self.coords)

    def get_clicked(self, mouseCoords):
        wh = self.size
        topx, topy = self.coords
        botx, boty= topx+wh, topy+wh
        if topx+(botx-topx) > mouseCoords[0] > topx and topy+(boty-topy) > mouseCoords[1] > topy:  
            return True
        return False

    def deactivate(self):
        self.surf = pygame.Surface((self.size,self.size), pygame.SRCALPHA)
        self.text = font.render(self.letter, True, (144,144,144))
        self.surf.blit(self.text,(self.surf.get_size()[0]//2-self.text.get_size()[0]//2,0))
        self.active = False
    
    def activate(self):
        self.surf = pygame.Surface((self.size,self.size), pygame.SRCALPHA)
        self.text = font.render(self.letter, True, self.color)
        self.surf.blit(self.text,(self.surf.get_size()[0]//2-self.text.get_size()[0]//2,0))
        self.active = True


class Main_text():
    def __init__(self, text, color):
        self.text = text
        self.color = color
        self.splitted_text = " ".join([x for x in text])
        self.placeholder = " ".join(["_" for x in range(len(self.text))])
        self.text_font = font.render(self.placeholder, True, self.color)
        self.surf = pygame.Surface(self.text_font.get_size(), pygame.SRCALPHA)
        self.surf.blit(self.text_font, (self.surf.get_size()[0]//2-self.text_font.get_size()[0]//2,0))

    def set_letter(self, letter):
        for index, x in enumerate(self.splitted_text):
            if x == letter:
                self.placeholder = self.placeholder[:index] + letter + self.placeholder[index + 1:]
        self.text_font = font.render(self.placeholder, True, self.color)
        self.surf = pygame.Surface(self.text_font.get_size(), pygame.SRCALPHA)
        self.surf.blit(self.text_font, (self.surf.get_size()[0]//2-self.text_font.get_size()[0]//2,0))
        
    def blit(self):
        screen.blit(self.surf, (WITHD//2-self.surf.get_size()[0]//2, 100))

    def get_success(self):
        if self.placeholder == self.splitted_text:
            return True
        return False


class AnyText():
    def __init__(self, text, color=(0,0,0), fontsize=20, boldfont=True, font_family="Arial", coords=(0,0), middle=False, corner=None):
        self.text = str(text)
        self.color = color
        self.fontsize = fontsize
        self.boldfont = boldfont
        self.font_family = font_family
        self.coords = coords
        self.middle = middle
        self.corner = corner # ur | ul | bl | br
        self.font = pygame.font.SysFont(self.font_family, self.fontsize) 
        self.font.set_bold(boldfont)
        self.text_font = self.font.render(self.text, True, self.color)
        self.surf = pygame.Surface(self.text_font.get_size(), pygame.SRCALPHA)
        self.surf.blit(self.text_font, (self.surf.get_size()[0]//2-self.text_font.get_size()[0]//2,0))

    
    def blit(self):
        if self.middle:
            screen.blit(self.surf, (self.coords[0]-self.surf.get_size()[0]//2, self.coords[1]-self.surf.get_size()[1]//2))
        elif self.corner == "ur": #upper right corner
            screen.blit(self.surf, (self.coords[0]-self.surf.get_size()[0], self.coords[1]))
        elif self.corner == "ul": #upper left corner
            screen.blit(self.surf, self.coords)
        elif self.corner == "bl": #bottom left corner
             screen.blit(self.surf, (self.coords[0], self.coords[1]-self.surf.get_size()[1]))
        elif self.corner == "br": #bottom rigt corner
             screen.blit(self.surf, (self.coords[0]-self.surf.get_size()[0], self.coords[1]-self.surf.get_size()[1]))
        else:
            screen.blit(self.surf, self.coords)
        
    def set_text(self,text):
        self.text_font = self.font.render(str(text), True, self.color)
        self.surf = pygame.Surface(self.text_font.get_size(), pygame.SRCALPHA)
        self.surf.blit(self.text_font, (self.surf.get_size()[0]//2-self.text_font.get_size()[0]//2,0))


class Gallow():
    def __init__(self):
        self.surf = pygame.Surface((360,310), pygame.SRCALPHA)#
        self.line_width = 5
        self.head_radius = 30
        self.rot_surf = pygame.Surface((self.line_width, 50), pygame.SRCALPHA)
        pygame.draw.rect(self.rot_surf, (0,0,0), (0,0, self.rot_surf.get_size()[0],self.rot_surf.get_size()[1])) 

    def reset(self):
        self.surf = pygame.Surface((360,310), pygame.SRCALPHA)
        pygame.draw.rect(self.surf, (0,0,0), (0,self.surf.get_size()[1]-self.line_width,self.surf.get_size()[0],self.line_width))

    def set_num(self, number):
        if number == 0:
            pygame.draw.rect(self.surf, (0,0,0), (0,self.surf.get_size()[1]-self.line_width,self.surf.get_size()[0],self.line_width))
        elif number == 1:
            pygame.draw.rect(self.surf, (0,0,0), (self.surf.get_size()[0]//2-self.line_width,0,self.line_width,self.surf.get_size()[1]))
        elif number == 2:
            pygame.draw.rect(self.surf, (0,0,0), (self.surf.get_size()[0]//2-self.line_width, 0 , self.surf.get_size()[0]//3, self.line_width))
        elif number == 3:
            self.surf.blit(pygame.transform.rotate(self.rot_surf, -45),(self.surf.get_size()[0]//2-self.line_width,0))
        elif number == 4:
            pygame.draw.rect(self.surf, (0,0,0), (self.surf.get_size()[0]//2+self.surf.get_size()[0]//3-self.line_width, 0 , self.line_width, 20))
        elif number == 5:
            pygame.draw.circle(self.surf,(0,0,0), (self.surf.get_size()[0]//2+self.surf.get_size()[0]//3-3, 20+self.head_radius), self.head_radius, width= self.line_width)
        elif number == 6:
            pygame.draw.rect(self.surf, (0,0,0), (self.surf.get_size()[0]//2+self.surf.get_size()[0]//3-self.line_width, 20+self.head_radius*2 , self.line_width, 100))
        elif number == 7:
            self.surf.blit(pygame.transform.rotate(self.rot_surf, 45),(self.surf.get_size()[0]//2+self.surf.get_size()[0]//3-self.line_width, 30+self.head_radius*2)) #right arm
        elif number == 8:
            self.surf.blit(pygame.transform.rotate(self.rot_surf, -45),(self.surf.get_size()[0]//2+self.surf.get_size()[0]//3-self.line_width-35, 33+self.head_radius*2)) # left arm
        elif number == 9:
            self.surf.blit(pygame.transform.rotate(self.rot_surf, 45),(self.surf.get_size()[0]//2+self.surf.get_size()[0]//3-self.line_width, 115+self.head_radius*2)) # right leg
        elif number == 10:
            self.surf.blit(pygame.transform.rotate(self.rot_surf, -45),(self.surf.get_size()[0]//2+self.surf.get_size()[0]//3-self.line_width-35, 115+self.head_radius*2)) #left leg
        
    def blit(self):
        screen.blit(self.surf,(420,190))


#letters' coordinates
X_axis = 275
coords = [
    (0+X_axis, 550), (50+X_axis, 550), (100+X_axis, 550), (150+X_axis, 550), (200+X_axis, 550), (X_axis+250, 550), (X_axis+300, 550), (X_axis+350, 550), (X_axis+400, 550), (X_axis+450, 550), (X_axis+500, 550), (X_axis+550, 550), (X_axis+600, 550), 
    (X_axis+0, 650), (X_axis+50, 650), (X_axis+100, 650), (X_axis+150, 650), (X_axis+200, 650), (X_axis+250, 650), (X_axis+300, 650), (X_axis+350, 650), (X_axis+400, 650), (X_axis+450, 650), (X_axis+500, 650), (X_axis+550, 650), (X_axis+600, 650)
    ] 

letters = []
for index, x in enumerate(LETTERS): #make each letter's class
    letters.append(Letter(x, (0,0,0), 50, coords[index]))

#left and right click checked
clicked_left = False
clicked_right = False

#set a random word from the valid words list
main_text = Main_text(random.choice(WORDS).upper(), (0,0,0))
print(main_text.text)

#make victory and defeate classes with details
victory_class = AnyText("VICTORY ROYALE",(252, 194, 3),fontsize=100, coords=(WITHD//2, HEIGHT//2),middle=True)
defeate_class = AnyText("DEFEATED",(209,0,0),fontsize=100, coords=(WITHD//2, HEIGHT//2),middle=True)

#point counter and instruction text class set
instruction_text = AnyText("Righ click to restart or press space")
points_class = AnyText(f"Row:{POINTS}", fontsize=30, coords=(WITHD, 0), corner="ur")

#init the gallow class 
gallow_class = Gallow()
gallow_class.set_num(MISSES)


victory = False
defeated = False
endphase = False

#main loop
while True:
           
    for event in pygame.event.get():
        if event.type == pygame.QUIT: sys.exit()

        if event.type == pygame.KEYDOWN:
            pressed_key = pygame.key.name(event.key)
            

            for letter in letters:
                # print()
                if pressed_key in ABC and endphase==False and letter.active == True: #check if pressed an english alphabet character

                    if letter.letter == pressed_key.upper(): #check if the pressed key is in the main word
                        if letter.letter in main_text.text :
                            main_text.set_letter(letter.letter)
                        else:
                            MISSES += 1
                            gallow_class.set_num(MISSES) #set gallow miss to show more sticks 
                        letter.deactivate() #deactivate the pressed letter
                        if main_text.get_success() == True:
                            victory = True
                            endphase = True
                            POINTS += 1
                            points_class.set_text(f"Row:{POINTS}") #set new row score
        
            if event.key == pygame.K_SPACE and endphase== True: #able to reset the game with space, if endphase
                victory = False
                defeated = False
                endphase = False
                MISSES = 0
                gallow_class.reset() #reset the gallow sticks
                main_text = Main_text(random.choice(WORDS).upper(), (0,0,0)) #set new word
                print(main_text.text)
                for letter in letters:
                    letter.activate() #activate the letters

    if pygame.mouse.get_pressed()[0] == 1  and clicked_left == False and victory==False and defeated == False: #check left click once and deactivate the letter if it not endphase
        
        clicked_left = True #if key press, disable
        for letter in letters:
            if letter.get_clicked(pygame.mouse.get_pos()) == True and letter.active == True: #same as keyboard press
                if letter.letter in main_text.text:
                    main_text.set_letter(letter.letter)
                else:
                    MISSES += 1
                    gallow_class.set_num(MISSES)
                letter.deactivate()
                if main_text.get_success() == True:
                    victory = True
                    endphase = True
                    POINTS += 1
                    points_class.set_text(f"Row:{POINTS}")
                    
    if pygame.mouse.get_pressed()[0] == 0 and clicked_left == True:
        clicked_left = False #if key released, enable
    if pygame.mouse.get_pressed()[2] == 1  and clicked_right == False and endphase == True: #same as space press
        print(pygame.mouse.get_pressed()[2], clicked_right, defeated)
        clicked_right = True #if key pressed, disable
        victory = False
        defeated = False
        endphase = False
        MISSES = 0
        gallow_class.reset()
        main_text = Main_text(random.choice(WORDS).upper(), (0,0,0))
        print(main_text.text)
        for letter in letters:
            letter.activate()

    if pygame.mouse.get_pressed()[2] == 0 and clicked_right == True:
        clicked_right = False #if key released, enable again


    screen.fill(backGroundColor) #set the background
    for index, letter in enumerate(letters):
        letter.blit()
    main_text.blit()    #show the main text
    points_class.blit()  #show the points
    gallow_class.blit()  #show the gallow

    #if the gallow is full, reset the points to 0 and set defeated endphase
    if MISSES == MAX_MISS:
        defeated = True
        endphase = True
        POINTS = 0
        points_class.set_text(f"Row:{POINTS}")

    #show vitroy or defeated text
    if victory:
        victory_class.blit()
        instruction_text.blit()
    if defeated:
        defeate_class.blit()
        instruction_text.blit()


    pygame.display.flip() # display update
    clock.tick(60) #set fps to 60